package AppAPI;

import java.util.ArrayList;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.specification.RequestSpecification;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import MainFiles.Main_Functions;

import com.relevantcodes.extentreports.LogStatus;

public class CreateAssessment extends Main_Functions{
	
@Test(priority=3)	
@SuppressWarnings("unchecked")
public void CreateAssessmentAPI()
{

	try
	{	
		RestAssured.baseURI = baseURI;
	RequestSpecification httpRequest = RestAssured.given();
	JSONObject requestParams = new JSONObject();
	Details.put(obj.getCellData("OustLabs", "BodyKey", 38), obj.getCellData("OustLabs", "BadyValue", 38));
	Details.put(obj.getCellData("OustLabs", "BodyKey", 39), obj.getCellData("OustLabs", "BadyValue", 39));
	Details3.put(obj.getCellData("OustLabs", "BodyKey", 41), obj.getCellData("OustLabs", "BadyValue", 41));
	httpRequest.header(obj.getCellData("OustLabs", "HeaderKey", 37), obj.getCellData("OustLabs", "HeaderValue", 37));
	httpRequest.header(obj.getCellData("OustLabs", "HeaderKey", 38), obj.getCellData("OustLabs", "HeaderValue", 38));
	httpRequest.cookies("set-cookies",cookie);
	requestParams.put(obj.getCellData("OustLabs", "BadyKey", 37), Details);
	requestParams.put(obj.getCellData("OustLabs", "BadyKey", 40), Details3);
	httpRequest.body(requestParams.toJSONString());
	response =  httpRequest.request(Method.POST, obj.getCellData("OustLabs", "Path", 37)) ;
	
	Body= response.getBody();
	apiBody = Body.asString();
	logger=report.startTest("Assessment API Testing");
	System.out.println(apiBody);
	logger.log(LogStatus.INFO, apiBody);
	requestParams.clear();
	if(response.statusCode()==200)
	{
		System.out.println("Assessment Expected Status Code :"+ response.getStatusCode());
		logger.log(LogStatus.PASS, String.valueOf(response.getStatusCode()) + " : Expected Status Code");
	}
	else
	{
		System.out.println("Assessment UnExpected Status Code :"+ response.getStatusCode());
		logger.log(LogStatus.FAIL, String.valueOf(response.getStatusCode()) + " : Unexpected Status Code");
	}
	
	
//	logger.log(LogStatus.INFO, obj.getCellData("OustLabs", "Path", 21));
//	
//	if(Body.asString().contains(obj.getCellData("OustLabs", "BodyValue", 21)))
//	{
//		logger.log(LogStatus.INFO, obj.getCellData("OustLabs", "Path", 21));
//		System.out.println("user Registered");
//		logger.log(LogStatus.PASS, obj.getCellData("OustLabs", "BodyValue", 22) + obj.getCellData("OustLabs", "BodyKey", 22));
//	}
//	else
//	{
//		
//		logger.log(LogStatus.INFO, "Body doesn't contain expected results");
//		System.out.println("user not Registered");
//		logger.log(LogStatus.FAIL, obj.getCellData("OustLabs", "BodyValue", 21) + " : isn't in the Body");
//	}

	 report.endTest(logger);
	 report.flush();
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	
}

   


}