package AppAPI;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.specification.RequestSpecification;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import MainFiles.Main_Functions;


public class RegistrationAPI extends Main_Functions{
	

	
@SuppressWarnings("unchecked")
@Test(priority = 2)
public void Test_RegistrationAPI()
{

	try
	{	
	RestAssured.baseURI = baseURI;
	RequestSpecification httpRequest = RestAssured.given();
	JSONObject requestParams = new JSONObject();
	
	httpRequest.header(obj.getCellData("OustLabs", "HeaderKey", 2), obj.getCellData("OustLabs", "HeaderValue", 2));
	httpRequest.header(obj.getCellData("OustLabs", "HeaderKey", 3), obj.getCellData("OustLabs", "HeaderValue", 3));

	httpRequest.cookie("api-key",cookie);
	
	for(int row=21;row <=22; row ++)
	{
		requestParams.put(obj.getCellData("OustLabs", "BodyKey", row), obj.getCellData("OustLabs", "BodyValue", row));
	}
	
//	requestParams.put("", "");
	
	response =  httpRequest.request(Method.PUT, obj.getCellData("OustLabs", "Path", 21)) ;
	Body= response.getBody();
	apiBody = Body.asString();
	logger=report.startTest("Registration API");

	logger.log(LogStatus.INFO, "Registration API : "+obj.getCellData("OustLabs", "Path", 21));
	requestParams.clear();
	if(response.statusCode()==200)
	{
		System.out.println(apiBody);
		System.out.println("Expected Status Code registration testing :" + response.getStatusCode());
		logger.log(LogStatus.INFO, "Expected Status Code registration api :" + response.getStatusCode() + obj.getCellData("OustLabs", "Path", 21));
		logger.log(LogStatus.PASS, String.valueOf(response.getStatusCode()) + " : Status Code");
	}
	else
	{
		System.out.println(apiBody);
		System.out.println("Unexpected Status Code registration testing :" + response.getStatusCode());
		logger.log(LogStatus.INFO, "Unexpected Status Code registration api :" + response.getStatusCode());
		logger.log(LogStatus.FAIL, String.valueOf(response.getStatusCode()) + " : Status Code");
	}
//	
//	
//	logger=report.startTest("Registration API Testing - Create User");
//	
//	if(ResponseBody.contains("usertest10"))
//	{
//		logger.log(LogStatus.INFO, obj.getCellData("OustLabs", "Path", 21));
//		System.out.println("user Registered");
//		logger.log(LogStatus.PASS, obj.getCellData("OustLabs", "BodyValue", 22) + obj.getCellData("OustLabs", "BodyKey", 22));
//	}
//	else
//	{
//		
//		logger.log(LogStatus.INFO, "Body doesn't contain expected results");
//		System.out.println("user not Registered");
//		logger.log(LogStatus.FAIL, obj.getCellData("OustLabs", "BodyValue", 21) + " : isn't in the Body");
//	}
	

//	//Existing user Data 
//	logger=report.startTest("Sign In API Status Code and Parameters Testing with InValid Data");
//
//	httpRequest.header(obj.getCellData("OustLabs", "HeaderKey", 2), obj.getCellData("OustLabs", "HeaderValue", 2));
//	httpRequest.header(obj.getCellData("OustLabs", "HeaderKey", 3), obj.getCellData("OustLabs", "HeaderValue", 3));
//	for(int row=34;row <=45; row ++)
//	{
//		requestParams.put(obj.getCellData("OustLabs", "BodyKey", row), obj.getCellData("OustLabs", "BodyValue", row));
//	}
//	
//	httpRequest.body(requestParams.toJSONString());
//	response =  httpRequest.request(Method.PUT, obj.getCellData("OustLabs", "Path", 21)) ;
//	ResponseBody= response.getBody().asString();
//	if(ResponseBody.contains(obj.getCellData("OustLabs", "BodyValue", 5)))
//	{
//		System.out.println("Does Response contains " + obj.getCellData("OustLabs", "BodyValue", 5)+" :"+response.asString().contains(obj.getCellData("OustLabs", "BodyValue", 5)));
//		logger.log(LogStatus.PASS, obj.getCellData("OustLabs", "BodyValue", 5) + " : Wrong Org ID is in the Body");
//	}
//	else
//	{
//		System.out.println("Body doesn't contain expected results");
//		logger.log(LogStatus.FAIL, obj.getCellData("OustLabs", "BodyValue", 3) + " : Wrong Org ID isn't in the Body");
//	}
//	if(response.statusCode()==200)
//	{
//		System.out.println("Expected Status Code :"+ response.getStatusCode());
//		logger.log(LogStatus.FAIL, String.valueOf(response.getStatusCode()) + " : Status Code");
//	}
//	else
//	{
//		System.out.println("Unexpected Status Code :"+ response.getStatusCode());
//		logger.log(LogStatus.PASS, String.valueOf(response.getStatusCode()) + " : Status Code");
//	}
//	
//	//Valid device and app versions 
//	logger=report.startTest("Sign In API with App version and Device information");
//	for(int row=9;row <=13; row ++)
//	{
//		requestParams.put(obj.getCellData("OustLabs", "BodyKey", row), obj.getCellData("OustLabs", "BodyValue", row));
//	}
//	httpRequest.body(requestParams.toJSONString());
//	response =  httpRequest.request(Method.PUT, obj.getCellData("OustLabs", "Path", 2)) ;
//	ResponseBody= response.getBody().asString();
//	if(response.statusCode()==200)
//	{
//		System.out.println("Expected Status Code :"+ response.getStatusCode());
//		logger.log(LogStatus.PASS, String.valueOf(response.getStatusCode()) + " : Status Code");
//	}
//	else
//	{
//		System.out.println("Unexpected Status Code :"+ response.getStatusCode());
//		logger.log(LogStatus.FAIL, String.valueOf(response.getStatusCode()) + " : Status Code");
//	}

	 report.endTest(logger);
	 report.flush();
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	
}

   


}