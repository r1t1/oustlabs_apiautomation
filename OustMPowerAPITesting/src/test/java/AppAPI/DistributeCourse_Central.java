package AppAPI;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.specification.RequestSpecification;

import org.testng.annotations.Test;

import MainFiles.Main_Functions;

import com.relevantcodes.extentreports.LogStatus;

public class DistributeCourse_Central extends Main_Functions{
	@SuppressWarnings("unchecked")
	@Test(priority=5)
	public void DistributeCourseCentralAPI()
	{
		

		try
		{	
		RestAssured.baseURI = baseURI;
		RequestSpecification httpRequest = RestAssured.given();
	
		
		
		Details.put(obj.getCellData("OustLabs", "BodyKey", 33), obj.getCellData("OustLabs", "BodyValue", 33));
		Details.put(obj.getCellData("OustLabs", "BodyKey", 34), obj.getCellData("OustLabs", "BodyValue", 34));
		httpRequest.header(obj.getCellData("OustLabs", "HeaderKey", 32), obj.getCellData("OustLabs", "HeaderValue", 32));
		httpRequest.header(obj.getCellData("OustLabs", "HeaderKey", 33), obj.getCellData("OustLabs", "HeaderValue", 33));
		httpRequest.cookies("set-cookies",cookie);
		for(int row=50;row <=52; row ++)
		{
			requestParams.put(obj.getCellData("OustLabs", "BodyKey", row), obj.getCellData("OustLabs", "BodyValue", row));
		}
		
		
		
		System.out.println("Distribution Central API");
		httpRequest.body(requestParams.toJSONString());
		response =  httpRequest.request(Method.POST, obj.getCellData("OustLabs", "Path", 50)) ;
		Body= response.getBody();
		logger=report.startTest("Distribution Central API Testing");
		logger.log(LogStatus.INFO, "Distribution Central API : "+obj.getCellData("OustLabs", "Path", 50));
		logger.log(LogStatus.INFO, Body.asString());
		logger.log(LogStatus.INFO, "Testing with valid data");
		requestParams.clear();
		if(response.statusCode()==200)
		{
			System.out.println("Expected Status Code :"+ response.getStatusCode());
			logger.log(LogStatus.PASS, String.valueOf(response.getStatusCode()) + " :Expected Status Code");	
		}
		else
		{
			System.out.println("unExpected Status Code :"+ response.getStatusCode());
		
			logger.log(LogStatus.FAIL, String.valueOf(response.getStatusCode()) + " : Unexpected Status Code");
		}
		
		
//		logger.log(LogStatus.INFO, "Distribution Central API Testing -- Valid Parameters Testing");
//	
//		if(Body.asString().contains(obj.getCellData("OustLabs", "BodyValue", 51)))
//		{
////			logger.log(LogStatus.INFO, "Course Created without any level(required parameter for distribution)-- course : " + obj.getCellData("OustLabs", "BodyValue", 33)+ obj.getCellData("OustLabs", "BodyValue", 34));
//			System.out.println("Distribution Central--API Pass");
//			logger.log(LogStatus.PASS, obj.getCellData("OustLabs", "BodyValue", 51));
//		}
//		else
//		{
//			
//			logger.log(LogStatus.INFO, "Body doesn't contain expected results");
//			System.out.println("Distribution Central__Fail");
//			logger.log(LogStatus.FAIL, obj.getCellData("OustLabs", "BodyKey", 51));
//		}
		 report.endTest(logger);
		 report.flush();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}

	   
}

