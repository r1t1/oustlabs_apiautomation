package AppAPI;

		

import org.testng.annotations.Test;







import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.specification.RequestSpecification;

import org.json.simple.JSONObject;

import MainFiles.Main_Functions;

import com.relevantcodes.extentreports.LogStatus;



public class OustAppSignInAPI extends Main_Functions{
	 		
	@SuppressWarnings("unchecked")
	@Test(priority = 1, description = "Sign in API testing with valid and invalid data.")
	public void Test_OustSigninAPI()
	{


		try
		{

		RestAssured.baseURI = baseURI;
		RequestSpecification httpRequest = RestAssured.given();
		JSONObject requestParams = new JSONObject();
		

		
		httpRequest.header(obj.getCellData("OustLabs", "HeaderKey", 2), obj.getCellData("OustLabs", "HeaderValue", 2));
		httpRequest.header(obj.getCellData("OustLabs", "HeaderKey", 3), obj.getCellData("OustLabs", "HeaderValue", 3));

		for(int row=2;row <=4; row ++)
		{
			requestParams.put(obj.getCellData("OustLabs", "BodyKey", row), obj.getCellData("OustLabs", "BodyValue", row));
		}
		
		
		httpRequest.body(requestParams.toJSONString());
		response =  httpRequest.request(Method.PUT, obj.getCellData("OustLabs", "Path", 2)) ;
		Body= response.getBody();
		apiBody = Body.asString();
		logger=report.startTest("Sign In API");
		logger.log(LogStatus.INFO, "Sign in API : "+obj.getCellData("OustLabs", "Path", 2));
		requestParams.clear();
		if(apiBody.contains(obj.getCellData("OustLabs", "BodyValue", 3)))
		{
			
			cookie = "tenantId=$Version=0;tenantId=zlBcAis/4FZPj41Ztay4ug==,api-key=$Version=0;api-key=5alPHj2hSgvPvNXciS5UBA==,userRole=$Version=0;userRole=OUST_SUPER_ADMIN,userId=$Version=0;userId=oustadmin25";			
			System.out.println("Does Response contains " + obj.getCellData("OustLabs", "BodyValue", 3)+" :"+response.asString().contains(obj.getCellData("OustLabs", "BodyValue", 3)));
			logger.log(LogStatus.PASS, obj.getCellData("OustLabs", "BodyValue", 3) + " : is in the Body");
			
		}
		else
		{
			System.out.println("Body doesn't contain expected results");
			logger.log(LogStatus.FAIL, obj.getCellData("OustLabs", "BodyValue", 3) + " : isn't in the Body");
		}
		
		logger.log(LogStatus.INFO, "Sign In API Status Code");
		if(response.statusCode()==200)
		{
			logger.log(LogStatus.INFO, apiBody);
			System.out.println("Expected Status Code  sign in:"+ response.getStatusCode());
			logger.log(LogStatus.PASS, String.valueOf(response.getStatusCode()) + " : Status Code");
		}
		else
		{
			System.out.println("Unexpected Status Code sign in:"+ response.getStatusCode());
			logger.log(LogStatus.FAIL, String.valueOf(response.getStatusCode()) + " : Status Code");
		}
		

		//Invalid Data 
		
		for(int row=3;row <=5; row ++)
		{
			requestParams.put(obj.getCellData("OustLabs", "BodyKey", row), obj.getCellData("OustLabs", "BodyValue", row));
		}
		
		
		httpRequest.body(requestParams.toJSONString());
		response =  httpRequest.request(Method.PUT, obj.getCellData("OustLabs", "Path", 2)) ;
		Body= response.getBody();
		apiBody = Body.asString();
		logger.log(LogStatus.INFO, "Sign in API with Invalid Data ");
		requestParams.clear();
		if(response.statusCode()==500)
		{
			logger.log(LogStatus.INFO, apiBody);
			System.out.println("UnExpected Status Code  sign in:"+ response.getStatusCode());
			logger.log(LogStatus.FAIL, String.valueOf(response.getStatusCode()) + " : Status Code");
		}
		else if (response.statusCode()==400)
		{
			logger.log(LogStatus.INFO, apiBody);
			System.out.println("Expected Status Code sign in:"+ response.getStatusCode());
			logger.log(LogStatus.PASS, String.valueOf(response.getStatusCode()) + " : Status Code");
		}
		else
		{
			logger.log(LogStatus.INFO, apiBody);
			System.out.println("UnExpected Status Code  sign in:"+ response.getStatusCode());
			logger.log(LogStatus.FAIL, String.valueOf(response.getStatusCode()) + " : Status Code");
		}
		
		
		

		 report.endTest(logger);
		 report.flush();
		requestParams.clear();
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	
   
    
}
