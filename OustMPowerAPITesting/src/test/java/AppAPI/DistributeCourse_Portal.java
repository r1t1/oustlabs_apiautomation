package AppAPI;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.specification.RequestSpecification;

import org.testng.annotations.Test;

import MainFiles.Main_Functions;



import com.fasterxml.jackson.databind.DeserializationFeature;
import com.relevantcodes.extentreports.LogStatus;

public class DistributeCourse_Portal extends Main_Functions{

	
	
	@SuppressWarnings("unchecked")
	@Test(priority=4)
	public void DistributeCourse_PortalAPI()
	{
		mapper.enable(DeserializationFeature.UNWRAP_ROOT_VALUE);
	
		try
		{	
		RestAssured.baseURI = baseURI;
		RequestSpecification httpRequest = RestAssured.given();

		httpRequest.header(obj.getCellData("OustLabs", "HeaderKey", 32), obj.getCellData("OustLabs", "HeaderValue", 32));
		httpRequest.header(obj.getCellData("OustLabs", "HeaderKey", 33), obj.getCellData("OustLabs", "HeaderValue", 33));
		httpRequest.cookies("set-cookies",cookie);
		requestParams.put(obj.getCellData("OustLabs", "BodyKey", 54), "["+obj.getCellData("OustLabs", "BodyValue", 54)+"]");
		
		System.out.println("Portal Distribution API");
		httpRequest.body(requestParams.toJSONString());
		response =  httpRequest.request(Method.POST, obj.getCellData("OustLabs", "Path", 54)) ;
		Body= response.getBody();
		logger=report.startTest("Portal Distribution API Testing");
		logger.log(LogStatus.INFO, "Portal Distribution : "+obj.getCellData("OustLabs", "Path", 54));
		logger.log(LogStatus.INFO, Body.asString());
		requestParams.clear();
		if(response.statusCode()==200)
		{
			System.out.println("Expected Status Code :"+ response.getStatusCode());
			logger.log(LogStatus.PASS, String.valueOf(response.getStatusCode()) + " :Expected Status Code");	
		}
		else
		{
			System.out.println("unExpected Status Code :"+ response.getStatusCode());
		
			logger.log(LogStatus.FAIL, String.valueOf(response.getStatusCode()) + " : Unexpected Status Code");
		}
		
//		
//		logger.log(LogStatus.INFO, "Create Course API Testing -- Valid Parameters Testing");
//	
//		if(Body.asString().contains(obj.getCellData("OustLabs", "BodyValue", 33)))
//		{
//			logger.log(LogStatus.INFO, "Course Created without any level(required parameter for distribution)-- course : " + obj.getCellData("OustLabs", "BodyValue", 33)+ obj.getCellData("OustLabs", "BodyValue", 34));
//			System.out.println("Course Created -- course");
//			logger.log(LogStatus.PASS, obj.getCellData("OustLabs", "BodyValue", 33));
//		}
//		else
//		{
//			
//			logger.log(LogStatus.INFO, "Body doesn't contain expected results");
//			System.out.println("Course Dint get created-- course");
//			logger.log(LogStatus.FAIL, obj.getCellData("OustLabs", "BodyKey", 32));
//		}
	
		 report.endTest(logger);
		 report.flush();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
}
